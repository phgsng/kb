Contents
-------------------------------------------------------------------------------

Layouts are grouped in subdirectories by type:

- *xkb* for X11 / XKB symbols,
- *kbd* for Linux console key mappings (``keymaps(5)``),
- *keyboardio* customization of the keyboard.io firmware.

