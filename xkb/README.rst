ph : everyday layout suitable for writing in numerous alphabets including
  English, German, Polish, various Scandinavian, French (involving the
  compose key)

sp : easy access to diacritics used in linguistics

ocs : full Old Church Slavonic / Old Russian layout based on the Russian
  phonetic layout

