// phg’s keyboardio customization.
// Adapted from the default keyboardio Model 01 firmware.
//
// Copyright 2016 Keyboardio, inc. <jesse@keyboard.io>
// See "LICENSE" for license details

#ifndef BUILD_INFORMATION
#define BUILD_INFORMATION "locally built"
#endif


/**
 * These #include directives pull in the Kaleidoscope firmware core,
 * as well as the Kaleidoscope plugins we use in the Model 01's firmware
 */


// The Kaleidoscope core
#include "Kaleidoscope.h"

#include "Kaleidoscope-Qukeys.h"

// Support for storing the keymap in EEPROM
#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"

#include "Kaleidoscope-LayerHighlighter.h"

// Support for communicating with the host via a simple Serial protocol
#include "Kaleidoscope-FocusSerial.h"

// Support for macros
#include "Kaleidoscope-Macros.h"

// Support for controlling the keyboard's LEDs
#include "Kaleidoscope-LEDControl.h"

// Support for "Numpad" mode, which is mostly just the Numpad specific LED mode
#include "Kaleidoscope-NumPad.h"

// Support for Keyboardio's internal keyboard testing mode
#include "Kaleidoscope-HardwareTestMode.h"

// Support for host power management (suspend & wakeup)
#include "Kaleidoscope-HostPowerManagement.h"

// Support for magic combos (key chords that trigger an action)
#include "Kaleidoscope-MagicCombo.h"

// Support for USB quirks, like changing the key state report protocol
#include "Kaleidoscope-USB-Quirks.h"

enum { MACRO_VERSION_INFO,
       MACRO_PHG_SECURE_PASSWORD,
       MACRO_ANY
     };

enum { PRIMARY, NUMPAD, FUNCTION, PHG_WINDOW };

#define PRIMARY_KEYMAP_PHG
// #define PRIMARY_KEYMAP_QWERTY

KEYMAPS(

#if defined (PRIMARY_KEYMAP_QWERTY)
  [PRIMARY] = KEYMAP_STACKED
  (___,          Key_1, Key_2, Key_3, Key_4, Key_5, Key_LEDEffectNext,
   Key_Backtick, Key_Q, Key_W, Key_E, Key_R, Key_T, Key_Tab,
   Key_PageUp,   Key_A, Key_S, Key_D, Key_F, Key_G,
   Key_PageDown, Key_Z, Key_X, Key_C, Key_V, Key_B, Key_Escape,
   Key_LeftControl, Key_Backspace, Key_LeftGui, Key_LeftShift,
   ShiftToLayer(FUNCTION),

   M(MACRO_ANY),  Key_6, Key_7, Key_8,     Key_9,         Key_0,         LockLayer(NUMPAD),
   Key_Enter,     Key_Y, Key_U, Key_I,     Key_O,         Key_P,         Key_Equals,
                  Key_H, Key_J, Key_K,     Key_L,         Key_Semicolon, Key_Quote,
   Key_RightAlt,  Key_N, Key_M, Key_Comma, Key_Period,    Key_Slash,     Key_Minus,
   Key_RightShift, Key_LeftAlt, Key_Spacebar, Key_RightControl,
   ShiftToLayer(FUNCTION)),

#elif defined (PRIMARY_KEYMAP_PHG)
  [PRIMARY] = KEYMAP_STACKED
  (___,          Key_1, Key_2, Key_3, Key_4, Key_5, Key_LEDEffectNext,
   Key_Backtick, Key_Q, Key_W, Key_E, Key_R, Key_T, Key_Tab,
   Key_PageUp,   Key_A, Key_S, Key_D, Key_F, Key_G,
   Key_PageDown, Key_Z, Key_X, Key_C, Key_V, Key_B, Key_CapsLock,

   Key_LeftShift,
   Key_LeftControl,
   Key_LeftAlt,
   Key_Spacebar,

   LockLayer(PHG_WINDOW),

   Key_Backspace, Key_6, Key_7, Key_8,     Key_9,         Key_0,         Key_RightBracket,
   Key_Enter,     Key_Y, Key_U, Key_I,     Key_O,         Key_P,         Key_LeftBracket,
                  Key_H, Key_J, Key_K,     Key_L,         Key_Semicolon, Key_Quote,
   LSHIFT(Key_RightAlt),  Key_N, Key_M, Key_Comma, Key_Period,    Key_Slash,     Key_Minus,

   Key_Spacebar,
   Key_RightAlt,
   Key_RightControl,
   Key_RightShift,

   LockLayer(FUNCTION)),

#else

#error "No default keymap defined. You should make sure that you have a line like '#define PRIMARY_KEYMAP_QWERTY' in your sketch"

#endif



  [NUMPAD] =  KEYMAP_STACKED
  (___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___,
   ___,

   M(MACRO_VERSION_INFO),  ___, Key_7, Key_8,      Key_9,              Key_KeypadSubtract, ___,
   ___,                    ___, Key_4, Key_5,      Key_6,              Key_KeypadAdd,      ___,
                           ___, Key_1, Key_2,      Key_3,              Key_Equals,         ___,
   ___,                    ___, Key_0, Key_Period, Key_KeypadMultiply, Key_KeypadDivide,   Key_Enter,
   ___, ___, ___, ___,
   ___),

  [FUNCTION] =  KEYMAP_STACKED
  (___,      Key_F1,           Key_F2,      Key_F3,     Key_F4,        Key_F5,           Key_CapsLock,
   M(MACRO_PHG_SECURE_PASSWORD),  ___,              Key_UpArrow,  ___,        ___, ___,  ___,
   Key_Home, Key_LeftArrow,       Key_DownArrow,  Key_RightArrow,  ___,  ___,
   Key_End,  Key_PrintScreen,  Key_Insert,  ___,        ___, ___,  Key_Escape,
   ___, Key_Delete, ___, ___,
   ___,

   Consumer_ScanPreviousTrack, Key_F6,                 Key_F7,                   Key_F8,                   Key_F9,          Key_F10,          Key_F11,
   Consumer_PlaySlashPause,    Consumer_ScanNextTrack, Key_LeftCurlyBracket,     Key_RightCurlyBracket,    Key_Equals, Key_RightBracket, Key_F12,
                               Key_LeftArrow,          Key_DownArrow,            Key_UpArrow,              Key_RightArrow,  ___,              ___,
   Key_PcApplication,          Consumer_Mute,          Consumer_VolumeDecrement, Consumer_VolumeIncrement, ___,             Key_Backslash,    Key_Pipe,
   ___, ___, Key_Enter, ___,
   UnlockLayer(FUNCTION)),

  [PHG_WINDOW] =  KEYMAP_STACKED
  (XXX,     LALT(Key_1), LALT(Key_2), LALT(Key_3), LALT(Key_4), LALT(Key_5), LockLayer(NUMPAD),
   XXX,             LALT(LSHIFT(Key_Q)),      XXX,      XXX,        XXX,        XXX,              XXX,
   XXX,             XXX,      XXX,      XXX,        XXX,        XXX,
   XXX,             XXX,      XXX,      LALT(LSHIFT(Key_C)),        XXX,        XXX, LALT(Key_CapsLock),
   ___,             ___,      ___,      ___,
   UnlockLayer(PHG_WINDOW),

   XXX,                 LALT(Key_6),   LALT(Key_7),  LALT(Key_8),  LALT(Key_9),  XXX,   LALT(Key_CapsLock),
   LALT(Key_Enter),     XXX,           XXX,          XXX,          XXX,          XXX,   XXX,
                        LALT(Key_H),   LALT(Key_J),  LALT(Key_K),  LALT(Key_L),  LALT(Key_RightBracket),   XXX,
   LSHIFT(Key_Insert),  XXX,           XXX,          XXX,          XXX,          LALT(Key_Slash),   XXX,
   ___,                 ___,           ___,          ___,
   XXX)
)

static void versionInfoMacro(uint8_t keyState) {
  if (keyToggledOn(keyState)) {
    Macros.type(PSTR("Keyboardio Model 01 - Kaleidoscope "));
    Macros.type(PSTR(BUILD_INFORMATION));
  }
}

static void anyKeyMacro(uint8_t keyState) {
  static Key lastKey;
  bool toggledOn = false;
  if (keyToggledOn(keyState)) {
    lastKey.keyCode = Key_A.keyCode + (uint8_t)(millis() % 36);
    toggledOn = true;
  }

  if (keyIsPressed(keyState))
    kaleidoscope::hid::pressKey(lastKey, toggledOn);
}

static void phg_secure_password (uint8_t keyState)
{
  if (keyToggledOn(keyState)) {
    Macros.type(PSTR("test1234"));
  }
}

LayerHighlighter highlight_phg_window (PHG_WINDOW);
LayerHighlighter highlight_function   (FUNCTION);

const macro_t *macroAction(uint8_t macroIndex, uint8_t keyState) {
  switch (macroIndex) {

  case MACRO_VERSION_INFO:
    versionInfoMacro(keyState);
    break;

  case MACRO_PHG_SECURE_PASSWORD:
    phg_secure_password(keyState);
    break;

  case MACRO_ANY:
    anyKeyMacro(keyState);
    break;
  }
  return MACRO_NONE;
}

void toggleLedsOnSuspendResume(kaleidoscope::plugin::HostPowerManagement::Event event) {
  switch (event) {
  case kaleidoscope::plugin::HostPowerManagement::Suspend:
    LEDControl.set_all_leds_to({0, 0, 0});
    LEDControl.syncLeds();
    LEDControl.paused = true;
    break;
  case kaleidoscope::plugin::HostPowerManagement::Resume:
    LEDControl.paused = false;
    LEDControl.refreshAll();
    break;
  case kaleidoscope::plugin::HostPowerManagement::Sleep:
    break;
  }
}

void hostPowerManagementEventHandler(kaleidoscope::plugin::HostPowerManagement::Event event) {
  toggleLedsOnSuspendResume(event);
}

static void toggleKeyboardProtocol(uint8_t combo_index) {
  USBQuirks.toggleKeyboardProtocol();
}

static void enterHardwareTestMode(uint8_t combo_index) {
  HardwareTestMode.runTests();
}

USE_MAGIC_COMBOS({.action = toggleKeyboardProtocol,
                  // Left Fn + Esc + Shift
                  .keys = { R3C6, R2C6, R3C7 }
}, {
  .action = enterHardwareTestMode,
  // Left Fn + Prog + LED
  .keys = { R3C6, R0C0, R0C6 }
});

KALEIDOSCOPE_INIT_PLUGINS(
  EEPROMSettings,
  EEPROMKeymap,

  Qukeys, /* https://github.com/keyboardio/Kaleidoscope-Qukeys */
  NumPad,

  Focus,
  FocusSettingsCommand,
  FocusEEPROMCommand,

  HardwareTestMode,

  LEDControl,
  LEDOff,

  highlight_phg_window,
  highlight_function,

  HostPowerManagement,

  Macros,
  MagicCombo,
  USBQuirks
);

void setup() {
  QUKEYS(
    kaleidoscope::plugin::Qukey(0, 3, 6, ShiftToLayer(PHG_WINDOW)), /* fn left */
    kaleidoscope::plugin::Qukey(0, 3, 9, ShiftToLayer(FUNCTION)),   /* fn right */
  )

  Qukeys.setTimeout(200);
  Qukeys.setReleaseDelay(20);

  Kaleidoscope.setup();

  NumPad.numPadLayer = NUMPAD;
  HardwareTestMode.setActionKey(R3C6);

  LEDOff.activate();
  EEPROMKeymap.setup(5);

  highlight_phg_window.color = CRGB(0x33, 0x11, 0xbb);
  highlight_phg_window.lockHue = 110;
  highlight_function.color = CRGB(0xcc, 0x44, 0x22);
  highlight_function.lockHue = 80;
}

void loop() {
  Kaleidoscope.loop();
}
